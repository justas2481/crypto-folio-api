<?php

namespace App\Service;

use App\Entity\Asset;

class CurrencyExchangeRatesCoinlayer implements CurrencyExchangeRatesInterface
{
    private const API_URL = 'http://api.coinlayer.com/';
    private const API_ENDPOINT_LIVE = 'live';
    private const CURRENCIES_RELATIONS = [
        'BTC' => Asset::CURRENCY_BTC,
        'ETH' => Asset::CURRENCY_ETH,
        'LTC' => Asset::CURRENCY_LTC,
    ];

    public function getRates(): array
    {
        $rates = $this->apiCall(
            self::API_ENDPOINT_LIVE,
            [
                'symbols' => implode(',', array_keys(self::CURRENCIES_RELATIONS)),
                ]
        );

        if (isset($rates['error'])) {
            return $rates;
        }

        $filteredRates = [];
        foreach ($rates['rates'] as $rateName => $rateValue) {
            if (array_key_exists($rateName, self::CURRENCIES_RELATIONS)) {
                $filteredRates[self::CURRENCIES_RELATIONS[$rateName]] = $rateValue;
            }
        }

        return $filteredRates;
    }

    private function apiCall(string $method, array $params = []): ?array
    {
        $params['access_key'] = $this->retrieveApiKey();
        $url = self::API_URL . $method . '?' .
            http_build_query($params);

        $curlHandle = curl_init($url);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curlHandle);
        curl_close($curlHandle);
        if ($response === false) {
            return ['error' => 'Unable to get response from API.'];
        }

        $rates = json_decode($response, true);
        if (
            $rates === null ||
            (isset($rates['success']) && $rates['success'] === 'false') ||
            (!isset($rates['rates']) || empty($rates['rates']))
        ) {
            return ['error' => 'For some reason API does not provide information about exchange rates.'];
        }

        return $rates;
    }

    // Retrieve from environment variable
    public function retrieveApiKey(): string
    {
        $apiKey = getenv('API_KEY_COINSLAYER');
        if (!$apiKey) {
            throw new \InvalidArgumentException('No API key provided for Coinslayer.');
        }

        return $apiKey;
    }
}
