<?php

namespace App\Service;

use App\Entity\Asset;
use App\Entity\User;
use App\Repository\AssetRepository;
use Doctrine\Common\Cache\Psr6\InvalidArgument;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class AssetManager
{
    private $manager;
    private $assetRepo;
    private $security;
    private $currencyExchangeApi;

    public function __construct(
        EntityManagerInterface $manager,
        Security $security,
        AssetRepository $assetRepo,
        CurrencyExchangeRatesInterface $exchangeApi
    ) {
        $this->manager = $manager;
        $this->assetRepo = $assetRepo;
        $this->security = $security;
        $this->currencyExchangeApi = $exchangeApi;
    }

    public function addOrEditAsset(
        ?int $id,
        string $label,
        int $currency,
        string $value
    ): bool {
        $user = $this->security->getUser();
        if ($id === null) { // New asset
            $asset = new Asset();
        } else {
            $asset = $this->assetRepo->findOneBy(['id' => $id, 'user' => $user]);
            if (!$asset) {
                return false;
            }
        }
        try {
            $asset->setLabel($label);
            $asset->setCurrency($currency);
            $asset->setVal($value);
            $asset->setUser($user);
        } catch (InvalidArgument $e) {
            return false;
        }

        $this->manager->persist($asset);
        $this->manager->flush();

        return true;
    }

    /**
     * @return Collection|Asset[]
     */
    public function getAllUserAssets(): Collection
    {
        /**
         * @var User $user
         */
        $user = $this->security->getUser();
        $assets = $user->getAssets();

        return $assets;
    }

    /**
     * @param Collection|Asset[] $assets
     */
    public function representJSONList(Collection $assets): array
    {
        $exchangeRates = $this->currencyExchangeApi->getRates();
        if (isset($exchangeRates['error'])) {
            return $exchangeRates;
        }

        $list = [];
        $list['total_assets_amount_in_usd'] =
            $this->calcTotalAssetsVal($assets, $exchangeRates);
        $list['assets'] = [];
        foreach ($assets as $asset) {
            $list['assets'][] = [
                'id' => $asset->getId(),
                'label' => $asset->getLabel(),
                'currency_name' => $asset->getCurrencyName(),
                'amount' => $asset->getVal(),
                'amount_in_usd' => $asset->calcValInDollars(
                    $exchangeRates
                ),
            ];
        }

        return $list;
    }

    public function deleteAsset(int $id): bool
    {
        /**
         * @var User $user
         */
        $user = $this->security->getUser();
        /**
         * @var Asset $asset
         */
        $asset = $this->assetRepo->findOneBy(['id' => $id, 'user' => $user]);
        if (!$asset) {
            return false;
        }

        $this->manager->remove($asset);
        $this->manager->flush();

        return true;
    }

    /**
     * @param Collection|Asset[] $assets
     */
    public function calcTotalAssetsVal(Collection $assets, array $exchangeRates): float
    {
        $sum = 0.0;
        foreach ($assets as $asset) {
            $sum += $asset->calcValInDollars($exchangeRates);
        }

        return $sum;
    }
}
