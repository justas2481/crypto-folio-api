<?php

namespace App\Service;

interface CurrencyExchangeRatesInterface
{
    public function retrieveApiKey(): string;
    public function getRates(): array;
    }
