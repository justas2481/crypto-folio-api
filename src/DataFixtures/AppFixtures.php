<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->passwordHasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $this->insertUsers($manager);

        $manager->flush();
    }

    private function insertUsers(ObjectManager $manager): void
    {
        $data = [
            [
                'email' => 'test1@example.com',
                'password' => 'test123',
            ],
            [
                'email' => 'test2@example.com',
                'password' => 'test456',
            ],
        ];

        foreach ($data as $userData) {
            $user = new User();
            $user->setEmail($userData['email']);
            $user->setPassword(
                $this->passwordHasher->hashPassword($user, $userData['password'])
            );
            $manager->persist($user);
        }
    }
}
