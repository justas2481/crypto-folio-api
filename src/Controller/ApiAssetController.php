<?php

namespace App\Controller;

use App\Entity\Asset;
use App\Service\AssetManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiAssetController extends AbstractController
{
    /**
     * @Route("/api/asset/new", name="api_asset_new")
     */
    public function new(AssetManager $manager, Request $request): Response
    {
        $currency = $request->query->get('currency', Asset::CURRENCY_BTC);
        $label = $request->query->get('label', 'Unknown');
        $value = $request->query->get('amount', '0');

        $isAdded = $manager->addOrEditAsset(null, $label, $currency, $value);
        if (!$isAdded) {
            return $this->json([
                'status' => 'ERROR',
                'message' => 'Unable to add new asset.',
            ]);
        }

        return $this->json([
            'status' => 'OK',
            'message' => 'Asset added.',
        ]);
    }

    /**
     * @Route("/api/asset/edit/{id}", name="api_asset_edit")
     */
    public function edit(
        Request $request,
        AssetManager $manager,
        int $id
    ): Response {
        $currency = $request->query->get('currency', Asset::CURRENCY_BTC);
        $label = $request->query->get('label', 'Unknown');
        $value = $request->query->get('amount', '0');

        $isEdited = $manager->addOrEditAsset($id, $label, $currency, $value);
        if (!$isEdited) {
            return $this->json([
                'status' => 'ERROR',
                'message' => 'Unable to edit asset.',
            ]);
        }

        return $this->json([
            'status' => 'OK',
            'message' => 'Asset updated.',
        ]);
    }

    /**
     * @Route("/api/asset/list", name="api_asset_list")
     */
    public function list(AssetManager $manager): Response
    {
        $jsonList = $manager->representJSONList(
            $manager->getAllUserAssets()
        );
        if (isset($jsonList['error'])) {
            return $this->json([
                'status' => 'ERROR',
                'message' => $jsonList['error'],
            ]);
        }

        return $this->json([
            'status' => 'OK',
            'message' => $jsonList,
        ]);
    }

    /**
     * @Route("/api/asset/delete/{id}", name="api_asset_delete")
     */
    public function delete(
        AssetManager $manager,
        int $id
    ): Response {
        $isDeleted = $manager->deleteAsset($id);
        if (!$isDeleted) {
            return $this->json([
                'status' => 'ERROR',
                'message' => 'There is no such asset.',
            ]);
        }

        return $this->json([
            'status' => 'OK',
            'message' => 'Asset deleted.',
        ]);
    }
}
