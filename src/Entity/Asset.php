<?php

namespace App\Entity;

use App\Repository\AssetRepository;
use Doctrine\Common\Cache\Psr6\InvalidArgument;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AssetRepository::class)
 */
class Asset
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $label;

    const CURRENCY_BTC = 1;
    const CURRENCY_ETH = 2;
    const CURRENCY_LTC = 3;

    const SUPPORTED_CURRENCIES = [
        self::CURRENCY_BTC,
        self::CURRENCY_ETH,
        self::CURRENCY_LTC,
    ];

    const CURRENCY_NAMES = [
        self::CURRENCY_BTC => 'BTC',
        self::CURRENCY_ETH => 'ETH',
        self::CURRENCY_LTC => 'LTC',
    ];

    /**
     * @ORM\Column(type="integer")
     */
    private $currency;

    /**
     * @ORM\Column(type="decimal", precision=32, scale=16)
     */
    private $val;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="assets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getCurrency(): ?int
    {
        return $this->currency;
    }

    public function getCurrencyName(): string
    {
        return self::CURRENCY_NAMES[$this->getCurrency()];
    }

    public function setCurrency(int $currency): self
    {
        if (!in_array($currency, self::SUPPORTED_CURRENCIES)) {
            throw new InvalidArgument("Unsupported currency.");
        }

        $this->currency = $currency;

        return $this;
    }

    public function getVal(): ?string
    {
        return $this->val;
    }

    public function setVal(string $val): self
    {
        if ((float) $val < 0) {
            throw new InvalidArgument("Can not store negative value of an asset.");
        }

        $this->val = $val;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function calcValInDollars(array $exchangeRates): float
    {
        $currency = (string) $this->getCurrency();
        if (!array_key_exists($currency, $exchangeRates)) {
            return 0.0;
        }

        return $this->getVal() * $exchangeRates[$currency];
    }
}
