<?php

namespace App\Tests\Entity;

use App\Entity\Asset;
use PHPUnit\Framework\TestCase;

class AssetTest extends TestCase
{
    private $asset;
    private $exchangeRates;

    public function setup(): void
    {
        $this->asset = new Asset();
        $this->asset->setCurrency(Asset::CURRENCY_BTC);
        $this->exchangeRates = [Asset::CURRENCY_BTC => 60000.00];
    }

    public function testCalcValInDollars(): void
    {
        $val = 0.2;
        $this->asset->setVal($val);
        $this->assertEquals(
            $this->exchangeRates[Asset::CURRENCY_BTC] * $val,
            $this->asset->calcValInDollars($this->exchangeRates),
            'Wrong calculation of Asset val when multiplying by exchange rate.'
        );

        // Fake exchange rate currency
        $exchangeRates = ['fake_currency' => 1.21];
        $this->assertEquals(
            0.0,   
            $this->asset->calcValInDollars($exchangeRates),
            'Unknown currency gets non-zero value.'
        );
    }

    public function testDenyNegativeVal()
    {
        $this->expectException('Doctrine\Common\Cache\Psr6\InvalidArgument');
        $this->asset->setVal(-1);
    }
}
