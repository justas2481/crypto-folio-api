Crypto Folio API

Task implementation (real-time checking of Crypto Assets based on REST API and CRUD).

# Project installation

Install `PHP` and needed extensions for this to work:

```bash
$ sudo apt install php php-json php-ctype php-curl php-mbstring php-xml php-zip php-tokenizer php-intl php-mysql libpcre3 --no-install-recommends
```

To make sure that `JWT` will work, be sure to install those (though might be installed already):

```bash
$ sudo apt install openssl && sudo apt install libcurl4-openssl-dev && sudo apt install libssl-dev
```

Check if `PHP` installed successfully:

```bash
$ php --version
```

Install `Symfony binary` with this command:

```bash
$ wget https://get.symfony.com/cli/installer -O - | bash
```

Then make it system-wide. In my case it is as follows:

```bash
$ sudo mv /home/justinas/.symfony/bin/symfony /usr/local/bin/symfony
```

Install `Composer` for dependency management. Instructions to do so: [In Composer's website](https://getcomposer.org/download/).
Check if `Composer` is installed:

```bash
$ composer --version
```

Check if everything works as it should:

```bash
$ symfony check:requirements
```

Install `MySQL server`:

```bash
$ sudo apt install mysql-server
```

Choose proper authentication method and set password:

```bash
$ sudo mysql_secure_installation
```

Connect to `MYSql server` as client:

```bash
$ sudo mysql -u root -p
```

When prompted, enter the password.
Check your `MySQL server version` if it is the same in `.env` file in the project. If it is not, make it to be the same as prompted.
Create a new `MySQL user`, because `root` can not be used with external apps than CLI:

```bash
$ CREATE USER '<your_username>'@'localhost' IDENTIFIED BY '<your_password>';
```

Give privileges to the new user:

```bash
$ GRANT ALL PRIVILEGES ON *.* TO '<your_username>'@'localhost' WITH GRANT OPTION;
```

Flush privileges:

```bash
$ FLUSH PRIVILEGES;
```

Then quit `MySQL CLI` with `\q` and move over to the project directory. In my case it is as follows:

```bash
$ cd ~/crypto_folio_api
```

Change database credentials to be correct in `.env` file. In my case they are as follows:
`DATABASE_URL="mysql://justinas:Justas123{{@127.0.0.1:3306/crypto_folio_api?serverVersion=8.0.27"`
Then download all the dependencies that project requires:

```bash
$ composer install
```

Create database for a project:

```bash
$ symfony console doctrine:database:create
```

Generate all needed `MySQL` tables:

```bash
$ symfony console doctrine:migrations:migrate
```

Insert fake users into the database:

```bash
$ symfony console doctrine:fixtures:load
```

Generate `JWT` keys for authentication and authorization:

```bash
$ symfony console lexik:jwt:generate-keypair
```

Run `Symfony local development server`:

```bash
$ symfony serve
```

Navigate to the project within your browser:
`http://localhost:8000/`

# Authentication

To authenticate and receive authorization token, make request like so:

```bash
$ curl -X POST -H "Content-Type: application/json" http://localhost:8000/api/login_check -d '{"username":"test1@example.com","password":"test123"}'
```

Or for another user:

```bash
$ curl -X POST -H "Content-Type: application/json" http://localhost:8000/api/login_check -d '{"username":"test2@example.com","password":"test456"}'
```

To use API service, pass received token as such:

```bash
$ curl -X GET http://localhost:8000/api/asset -H "Authorization: Bearer {token_here}"
```

`(Enter without {}).`

I chose to use Coinslayer service for REST API and decided to keep API key as an environment variable, so please set your API key for the service as follows:

```bash
$ export API_KEY_COINSLAYER="your_key_here"
```

Then reload `Symfony server`.

# Considerations

* Validation could definitely be improved by using annotations and some bundles, but that would take reasonably considerable amount of time, so in general I just rely on uncaught exceptions here mostly.
* I didn't include functional or integration tests, they would definitely help here.
* Although I added 1 Unit test, this was just ment to show that I understand basics of it.
* Another weak point of an API that I know about is that I implemented everything (except authorization) to rely on GET method, where as we know these days API should use Post, Put, Patch as well for their corresponding actions.
* I use IDs instead of UUIDs which could be better in terms of improvement.
* Error mechanism should return errors in a consistent manner with constant values that reflect specific meaning.

# Quick note how to invoke corresponding API actions

Everything is based on routes and those are as follows:

`/api/asset/new` - adds a new `asset` which is linked to the specific user (authenticated via JWT).
Parameters (passed as a `query` string):
1. currency (1 = BTC, 2 = ETH, 3 = LTC).
1. Label of an asset.
1. Amount (how much this particular Asset is worth while).

`/api/asset/edit/{asset_id}` - modifies an existing Asset. Parameters are the same as above when creating new Asset.

`/api/asset/list` - returns information about the existing Assets and their worth in USD based on exchange rates from external API service.

`/api/asset/delete/{asset_id}` - removes asset from the database (asset should belong to the current user).
